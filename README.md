#为什么开发MyMVC

希望借助于Netty强大的NIO能力来打造一款轻量级、性能高的HTTP服务器。

同时，并不会屏蔽技术原理，使得用户知其然且知其所以然。



#MyMVC的技术方案

网络IO框架:   Netty

URL映射方案:  参考Jetty的映射规则(full match,prefix match,suffix match)

动态页面渲染： 已经支持Velocity模板,后续会考虑支持freemarker,jsp.

              支持开发模式下自动探测Velocity的变化，不用重启服务器

访问静态资源:  支持




#从MyMVC中可以学到什么？

1)如何用Netty开发健壮的程序

2)如何使用Netty开发业务耗时的系统

3)深入理解MVC的本质

4)理解Velocity的渲染及结果输出

对你理解同类型web服务器:Tomcat | Play | Jetty等有很大的指导意义。



#学习交流

适用人群:Netty爱好者、技术Geek, 对代码有控制欲的程序员，喜欢研究技术本质的人。

官方群: 206527503






![输入图片说明](http://git.oschina.net/uploads/images/2016/0521/143229_3b79175d_70679.png "在这里输入图片标题")


扩展阅读：

http://www.open-open.com/lib/view/open1453342311292.html (Apache Velocity开发者指南)-强烈推荐！


Velocity-http://www.ibm.com/developerworks/cn/java/j-lo-velocity1/

http://www.open-open.com/lib/view/open1388891400250.html

http://www.open-open.com/lib/view/open1395753723806.html

http://www.open-open.com/lib/view/open1454503732995.html (freemarker)

http://www.open-open.com/lib/view/open1453451157823.html (freemarker)

http://ecomfe.github.io/esf/demo/ (百度的一个CSS框架)