package com.freedom.web.mymvc.pool;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程创建工厂
 * 
 * @author 刘国庆 2016年5月17日
 *
 */
public class DefaultThreadFactory implements ThreadFactory {

	private String poolName;

	private final AtomicInteger nextId = new AtomicInteger();

	public DefaultThreadFactory(String poolName) {
		this.poolName = poolName + "-";
	}

	public Thread newThread(Runnable r) {
		Thread t = new Thread(r, poolName + nextId.incrementAndGet());
		t.setDaemon(true);
		return t;
	}

}
