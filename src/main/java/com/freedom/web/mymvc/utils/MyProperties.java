package com.freedom.web.mymvc.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
/**
 * 
 * @author zhiqiang.liu
 * @2016年5月16日
 *
 */
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

public class MyProperties {
	private static final Logger logger = LogManager.getLogger(MyProperties.class);

	// 以下为全局需要

	private static MyProperties myProperties = null;// 全局单例变量，一开始就存在

	static {// 静态块里，只加载一次

		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(MyConstants.CONFIG_FILE));
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(MyConstants.CONFIG_FILE);
			props.load(in);
			in.close();
		} catch (Exception e) {
			LoggerUtils.error(logger, "fail to read config file " + MyConstants.CONFIG_FILE);
			System.exit(-1);
		}
		// 读取值
		LoggerUtils.debug(logger, "succeed to read config file " + MyConstants.CONFIG_FILE);
		// netty
		int netty_port = Integer.parseInt(props.getProperty(MyConstants.NETTY_PORT, "10000"));
		int netty_boss = Integer.parseInt(props.getProperty(MyConstants.NETTY_BOSS, "1"));
		int netty_worker = Runtime.getRuntime().availableProcessors()
				* Integer.parseInt(props.getProperty(MyConstants.NETTY_WORKER, "2").trim());// 2倍cpu
		// consumer
		int consumer_worker = Runtime.getRuntime().availableProcessors()
				* Integer.parseInt(props.getProperty(MyConstants.CONSUMER_WORKER, "6").trim());// 6倍cpu

		// max size:default 1M
		int max_size_of_http_request = Integer
				.parseInt(props.getProperty(MyConstants.MAX_SIZE_OF_HTTP_REQUEST, "1048576").trim());

		props = null;
		// 构造新的对象
		myProperties = new MyProperties(netty_port, netty_boss, netty_worker, consumer_worker,
				max_size_of_http_request);
		LoggerUtils.debug(logger, "succeed to create my properties object ");
	}

	public static MyProperties getInstance() {
		return myProperties;
	}

	// 私有属性开始//////////////////////////////////////////////////////////////////
	// netty
	private int nettyPort;
	private int nettyBoss;
	private int nettyWorker;
	// consumer worker
	private int consumerWorker;
	// max size
	private int maxSize;

	private MyProperties() {// 私有方法，保证单例

	}

	private MyProperties(int np, int nboss, int nworker, int cworker, int mSize) {

		// used by netty
		this.nettyPort = np;
		this.nettyBoss = nboss;
		this.nettyWorker = nworker;
		this.consumerWorker = cworker;
		this.maxSize = mSize;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public int getConsumerWorker() {
		return this.consumerWorker;
	}

	public int getNettyPort() {
		return nettyPort;
	}

	public int getNettyBoss() {
		return nettyBoss;
	}

	public int getNettyWorker() {
		return nettyWorker;
	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder("\n");
		strBuilder.append(MyConstants.NETTY_PORT).append(": ").append(nettyPort).append("\n");
		strBuilder.append(MyConstants.NETTY_BOSS).append(": ").append(nettyBoss).append("\n");
		strBuilder.append(MyConstants.NETTY_WORKER).append(": ").append(nettyWorker).append("\n");
		strBuilder.append(MyConstants.CONSUMER_WORKER).append(": ").append(consumerWorker).append("\n");
		strBuilder.append(MyConstants.MAX_SIZE_OF_HTTP_REQUEST).append(": ").append(maxSize).append("");
		//
		return strBuilder.toString();
	}

	// 测试
	public static void main(String[] args) {
		// just for test
		MyProperties property = MyProperties.getInstance();
		logger.debug(property.toString());
	}
}
