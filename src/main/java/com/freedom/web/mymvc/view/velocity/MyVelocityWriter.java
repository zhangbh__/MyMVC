package com.freedom.web.mymvc.view.velocity;

import java.io.IOException;

public class MyVelocityWriter extends java.io.Writer {

	private StringBuilder builder = new StringBuilder("");// 单线程，线程安全

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		// 一次请求中，会多次被调用，所以需要先聚集起来
		builder.append(cbuf, off, len);
	}

	@Override
	public void flush() throws IOException {
		// TODO Auto-generated method stub
		// 目前不认为有必要实现
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		// 目前不认为有必要实现
	}

	public String getBody() {
		String body = builder.toString();
		builder = null;
		return body;
	}

}
